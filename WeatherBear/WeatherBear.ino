/*
 * WeatherBear
 * this sketch connects to a mqtt broker, and sends the windspeed to MQTT
 * ever n time depending on the values of SendSpeedTimeout & SpeedCalc
 * 
 * (c) by Hartmut Eilers <hartmut@eilers.net>
 * released under the Terms of the GNU GPL 2.0 or later
 * 
 * used libraries:
 * https://github.com/zeitgeist87/RFReceiver
 */

#define DEBUG_SERIAL                                                // use serial for debugging info
#define LOCAL_HARDWARE                                              // the hardware is attached to the ESP
//#undef LOCAL_HARDWARE                                             // the hardware is attached to a ATTINY
#define HEARTBEAT                                                   // if defined a Heartbeat is send to MQTT
//#undef HEARTBEAT                                                  // if defined a Heartbeat is send to MQTT

#include <ESP8266WiFi.h>
#include <PubSubClient.h>                                           // MQTT client library
#include <Ticker.h>                                                 // Ticker Library ( for timetriggers )
#include <PinChangeInterruptHandler.h>                              // used by RFReceiver
#include <RFReceiver.h>                                             // 433 MHZ Receiver

                                                                    // set according to your hardware
#define SPEEDPIN 16                                                 // the io pin where the pulse is connected
#define PULSEPERROUND 3                                             // my speedmeter sends 3 pulses per round
float radius=0.107;                                                 // radius of windmeter in meters 

#define RECEIVEPIN 5                                                // 433 MHz receiver

                                                                    // wifi settings
#define SSID "hucky.net"                                            // insert your SSID
#define PASS "internet4hucky"                                       // insert your password
boolean ConnectionLost=false;                                       // lost connection flag

String ESP_Id = ""; 

                                                                    // MQTT related settings, set your settings here
const char* mqtt_server           = "mqtt.hucky.net";
const char* mqtt_username         = "hartmut";
const char* mqtt_pass             = "p4hatmqtt";
const uint16_t mqtt_port          = 1883;

                                                                    // the following values can be left as is, suitable for a quick test
String mqtt_id                    = "WeatherBear-";
String topic_mqtt_tmpl            = "HC/ESPID/WeatherBear/";
String MQTT_ID;
#define topiclength 50                                              // limit topics in length

uint16_t pulseCnt;                                                  // the counting var for the pulses
int val;                                                            // current value of input pin
int OldVal;                                                         // the value of input pin in the last measurement

#ifndef LOCAL_HARDWARE
RFReceiver receiver(RECEIVEPIN);                                    // Listen on digital pin RECEIVEPIN
#endif

WiFiClient WeatherBear;                                             // the weather station client
PubSubClient mqtt_client(WeatherBear);

Ticker SendSpeedTicker;                                             // callback routine to calculate the speed
#define SendSpeedTimeout 5                                          // measure for n secs
#define SpeedCalc 12                                                // multiply by SpeedCalc to get the amount in a minute

#ifdef HEARTBEAT
Ticker MQTTheartbeat;                                               // wake up to send heartbeat
#define MQTTHEARTBEATTIMEOUT 30                                     // currently send heartbeat after 30 seconds
#endif

void ICACHE_RAM_ATTR sendMQTTheartbeat () {                         // send a MQTT message as heartbeat
  String TempTopic = topic_mqtt_tmpl;                               // replace ESPID with real value
  TempTopic.replace("ESPID", ESP_Id);
  TempTopic = TempTopic + "alive";                                  // add the heartbeat topic
  char PubTempTopic[topiclength];
  TempTopic.toCharArray(PubTempTopic,TempTopic.length()+1);
  sendmqtt (PubTempTopic,"1");                                      // send the MQTT message
#ifdef HEARTBEAT
  //MQTTheartbeat.attach(MQTTHEARTBEATTIMEOUT, sendMQTTheartbeat);  // restart the timer
#endif
#ifdef DEBUG_SERIAL
  Serial.println("heartbeat send over MQTT");
#endif
}

void ICACHE_RAM_ATTR SendMQTTSpeed () {                             // send the data over MQTT
  float rpm=pulseCnt/PULSEPERROUND*SpeedCalc;                       // calc the round per minute
  float windspeed=rpm/60*2*PI*radius;                               // take rpm and calculate the speed in m/s
#ifdef DEBUG_SERIAL
  Serial.print("pulses=");                                          // debugging output
  Serial.print(pulseCnt);        
  Serial.print(" speed=");
  Serial.print(rpm);
  Serial.print(" rpm  ");
  Serial.print("speed=");                                           
  Serial.print(windspeed);
  Serial.println(" m/s");
#endif
  String TempTopic = topic_mqtt_tmpl;                               // prepare and publish over MQTT
  TempTopic.replace("ESPID", ESP_Id);                               // replace and prepare topic
  TempTopic = TempTopic + "windspeed";
  char PubTempTopic[topiclength];
  TempTopic.toCharArray(PubTempTopic,TempTopic.length()+1);
  char result[8];                                                   // Buffer big enough for 7-character float
  dtostrf(windspeed, 5, 2, result);                                 // convert value to string
  if (WiFi.status() == WL_CONNECTED) {                              // check the network connection
    sendmqtt (PubTempTopic,result);                                 // publish over MQTT
  }
  pulseCnt=0;                                                       // reset for new measurement
}

boolean sendmqtt(char* topic,char *value) {                         // send a value to topic on mqtt broker
  
  boolean sendmqtt = false;
  boolean state =false;
  while ( !sendmqtt ) {
#ifdef DEBUG_SERIAL        
    Serial.print("publishing to topic: ");
    Serial.println(topic);
#endif    
    state=mqtt_client.publish(topic, value, true);                  // publish the converted value ( retained )
    if ( !state ) {                                                 // on error reconnect to MQTT broker
#ifdef DEBUG_SERIAL                                               
      Serial.print (mqtt_client.state());
      Serial.println(" MQTT publish Error");
#endif
    }
    sendmqtt = true;                                                // successful published to topic
  }
}

boolean mqttConnect(const char* my_mqtt_server, uint16_t my_mqtt_port, const char* my_mqtt_username, const char* my_mqtt_pass){

  mqtt_client.setClient(WeatherBear);
  mqtt_client.setServer(my_mqtt_server, my_mqtt_port);
  // apend the unique ID of the ESP to the mqtt_id to ensure unique ID
  MQTT_ID=mqtt_id + ESP_Id;
  boolean state=mqtt_client.connect(MQTT_ID.c_str(),my_mqtt_username,my_mqtt_pass);
  if ( state ) {
#ifdef DEBUG_SERIAL        
    Serial.println("connected to mqtt");
#endif
    return true;
  } else {
#ifdef DEBUG_SERIAL        
    Serial.println("MQTT connection Error");
    Serial.println(mqtt_client.state());
#endif
    return false;
  }
  
}

void ConnectNetwork() {
#ifdef DEBUG_SERIAL        
  Serial.println("startup Wifi");
#endif
  if ( ConnectionLost ) {                                           // turn off the complete wifi
    WiFi.mode(WIFI_OFF);
    WiFi.forceSleepBegin();
    delay(1);                                                       // Needed, at least in my tests WiFi doesn't 
                                                                    // power off without this for some reason
    WiFi.forceSleepWake();                                          // turn it on again
    WiFi.mode(WIFI_STA);  
    wifi_station_connect();
  }
  WiFi.begin(SSID, PASS);                                           // Connect to WiFi network
  int connectCnt=0;
  while (WiFi.status() != WL_CONNECTED) {                           // Wait for connection
    delay(50);
    connectCnt++;
#ifdef DEBUG_SERIAL        
    Serial.print(".");
#endif
    if ( connectCnt>=100 ) {                                        // retried too often -> restart system
#ifdef DEBUG_SERIAL        
      Serial.println ("No Wifi Connection - giving up, Reset....");
#endif
      ESP.restart();
    }
  }
#ifdef DEBUG_SERIAL        
  Serial.print("connected to Wifi network: ");
  Serial.println(SSID);
#endif
                                                                    // MQTT setup
#ifdef HEARTBEAT
  MQTTheartbeat.attach(MQTTHEARTBEATTIMEOUT, sendMQTTheartbeat);    // send MQTT heartbeat after that time
#endif
  int result = mqttConnect(mqtt_server, mqtt_port, mqtt_username, mqtt_pass);  // connect to MQTT broker
  ConnectionLost=false;                                             // reset connection lost flag

}

void setup() {
#ifdef DEBUG_SERIAL        
  Serial.begin(115200);                                             // full speed to monitor
#endif
  delay(1000);
  ESP_Id=ESP.getChipId();
#ifdef DEBUG_SERIAL        
  Serial.println("WeatherBear MQTT Weather Station");
  Serial.print("connect as MQTT ID: ");
  Serial.println(ESP_Id);
#endif 

  ConnectionLost=false;
  ConnectNetwork();                                                 // establish Wifi connection and attach to MQTT
  
#ifdef LOCAL_HARDWARE
  pinMode(SPEEDPIN,INPUT);                                          // GPIO input pin where the pulse are connected
#endif 
  
                                                                    // SpeedCount Setup
  SendSpeedTicker.attach(SendSpeedTimeout, SendMQTTSpeed);          // every SendSpeedTimeout the value is calculated

  pulseCnt=0;
  OldVal=0;
#ifndef LOCAL_HARDWARE
  receiver.begin();                                                 // enable 433MHz receiver
#endif
}

void loop() {

  mqtt_client.loop();                                               // process MQTT events

  if (WiFi.status() != WL_CONNECTED) {                              // check the network connection
#ifdef DEBUG_SERIAL        
    Serial.println("Wifi connection lost");                         // Lost -> reconnect
#endif
    ConnectNetwork();                                               //ConnectionLost=true;
  }

#ifdef LOCAL_HARDWARE
  
  val = digitalRead(SPEEDPIN);                                      // read the input

  if ( !OldVal && val ) {                                           // detect rising edge
    pulseCnt++;
  }
  
  OldVal=val;                                                       // save value for next loop
#endif

#ifndef LOCAL_HARDWARE
  // read the 433 MHz receiver
  char msg[MAX_PACKAGE_SIZE];
  byte senderId = 0;
  byte packageId = 0;
  if (receiver.ready()) { 
    byte len = receiver.recvPackage((byte *)msg, &senderId, &packageId); 
    Serial.println("");
    Serial.print("Package: ");
    Serial.println(packageId);
    Serial.print("Sender: ");
    Serial.println(senderId);
    Serial.print("Message: ");
    Serial.println(msg);
    pulseCnt=atoi(&msg[0]);
    SendMQTTSpeed ();
  } 
#endif
}
