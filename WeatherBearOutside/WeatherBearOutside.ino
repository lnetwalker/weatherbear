/*  
 *  WeatherBearOutside
 *  ermittelt/misst sensorwerte und funkt sie via 433MHz zur Basis
 *  
 *  (c) 2020 by Hartmut Eilers <hartmut@eilers.net>
 *  released under the terms of the GNU GPL 2.0 or later
 */

#include <RFTransmitter.h>

#define TRANSMITPIN 1                                               // the 433MHz sender
#define NODE_ID 2                                                   // nodeId in 433MHz network

// set according to your hardware
#define SPEEDPIN 3                                                  // the io pin where the pulse is connected

uint16_t pulseCnt;                                                  // the counting var for the pulses
int val;                                                            // current value of input pin
int OldVal;                                                         // the value of input pin in the last measurement

RFTransmitter transmitter(TRANSMITPIN, NODE_ID);                    // a new sender

#define SendSpeedTimeout 5                                          // measure windspeed for n secs
unsigned long TimeNow, TimeLast;

void setup() {

  pinMode(SPEEDPIN,INPUT);                                          // GPIO input pin where the pulse are connected

  pulseCnt=0;
  OldVal=0;

  TimeLast=millis();
}

void loop() {

  TimeNow=millis();

  val = digitalRead(SPEEDPIN);                                      // read the input

  if ( !OldVal && val ) {                                           // detect rising edge
    pulseCnt++;
  }

  OldVal=val;                                                       // save value for next loop

  if ( (TimeNow-TimeLast) > ( SendSpeedTimeout * 1000 ) ) {         // Zeit abgelaufen, nächsten Wert senden

    char result[8]="       ";                                       // Buffer big enough for 7-character float
    dtostrf(pulseCnt, 7, 0, result);                                // generate String from Speed

    char *msg = result;
    transmitter.send((byte *)msg, strlen(msg) + 1);                 // send the speed over 433 MHz link
    transmitter.resend((byte *)msg, strlen(msg) + 1);               // resend the value to be sure it's received

    pulseCnt=0;                                                     // reset counter var
    TimeLast=TimeNow;
  }
}
